package com.atum.bel.view.projects

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener

class ProjectApiClient (context: Context, listener: ApiResultListener) : ApiClient(context,listener) {

    fun getProjects()
    {
        createGetRequest("project")
    }

    fun sreachInProjects(key:String)
    {
        createGetRequest("project/" + key)
    }
}