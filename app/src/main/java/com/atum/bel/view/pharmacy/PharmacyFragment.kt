package com.atum.bel.view.pharmacy

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.atum.bel.R
import com.atum.bel.adapter.PharmacyAdapter
import com.atum.bel.api.ApiResultListener
import com.atum.bel.common.base.BaseFragment
import com.atum.bel.model.Pharmacy.PharmacyModel
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_pharmacy.*





class PharmacyFragment(val ahBottomNavigation: AHBottomNavigation, val supportFragmentManager: FragmentManager) : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        init(ahBottomNavigation,supportFragmentManager)

        return inflater.inflate(R.layout.fragment_pharmacy, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        callService()

    }

    private fun callService() {
        val listener = object : ApiResultListener{
            override fun succeed(fromJson: String) {

                val gson =Gson()
                setUpAdapter(gson.fromJson(fromJson,PharmacyModel.Response::class.java).result)
            }

            override fun fail(errorMessage: String?) {

                Toast.makeText(context,errorMessage,Toast.LENGTH_LONG).show()
            }
        }

        PharmacyApiClient(context!!,listener).listPharmacies()
    }

    private fun setUpAdapter( list: MutableList<PharmacyModel>)
    {
        rvPharmacies.layoutManager = LinearLayoutManager(activity)
        rvPharmacies.adapter = PharmacyAdapter(list, object : PharmacyAdapter.OnItemClickListener
        {
            override fun onItemClick(pharmacy: PharmacyModel) {

                val uri = "google.navigation:q=".plus(pharmacy.address.replace(" ","+"))
                val gmmIntentUri = Uri.parse(uri)

                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                startActivity(mapIntent)


            }
        })

    }

    fun showMap(gmmIntentUri: Uri) {
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }
}
