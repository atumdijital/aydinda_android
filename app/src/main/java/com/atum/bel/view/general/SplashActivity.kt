package com.atum.bel.view.general

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.atum.bel.R
import com.atum.bel.view.auth.AuthActivity
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class SplashActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Executors.newSingleThreadScheduledExecutor().schedule({
            startMainActivity()
        }, 5, TimeUnit.SECONDS)
    }


    fun startMainActivity()
    {
        startActivity(Intent(this, AuthActivity::class.java))
        this.finish()
    }

}
