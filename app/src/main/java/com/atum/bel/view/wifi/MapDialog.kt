package com.atum.bel.view.wifi

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.atum.bel.R
import com.atum.bel.model.Wifi.WifiModel
import kotlinx.android.synthetic.main.fragment_map_dialog.*






class MapDialog(var point:WifiModel) : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_map_dialog, container, false)
        return v

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtWifiAddress.text = point.description

        btnGoToWifi.setOnClickListener(
                {
                    val uri =  "http://maps.google.com/maps?daddr=".plus(point.location.get(0)).plus(",").plus(point.location.get(1))
                    showMap(Uri.parse(uri))

                }
        )
    }

    fun showMap(gmmIntentUri: Uri) {
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }
}