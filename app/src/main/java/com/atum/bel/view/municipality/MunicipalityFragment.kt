package com.atum.bel.view.municipality

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.recyclerview.widget.LinearLayoutManager
import com.atum.bel.R
import com.atum.bel.adapter.ServicesAdapter
import com.atum.bel.api.ApiResultListener
import com.atum.bel.common.base.BaseFragment
import com.atum.bel.model.Municipality.MunicipalityModel
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_municipality.*


class MunicipalityFragment(val ahBottomNavigation: AHBottomNavigation) : BaseFragment() {

    private var list: MutableList<MunicipalityModel>? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        init(ahBottomNavigation,activity?.supportFragmentManager)
        return inflater.inflate(R.layout.fragment_municipality, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnBack.setOnClickListener {
            onBackPress()
        }

        val servicesApiCallListener = object : ApiResultListener{
            override fun succeed(fromJson: String) {

                val gson =  Gson()
                val result : MutableList<MunicipalityModel>? = gson.fromJson(fromJson, MunicipalityModel.MunicipalityResponseModel::class.java).result
                list = result
                setAdapter()
            }

            override fun fail(errorMessage: String?) {
                Toast.makeText(context,errorMessage, Toast.LENGTH_LONG).show()
            }

        }
        MunicipalityApiClient(context!!,servicesApiCallListener).getServices()
    }

    private fun setAdapter() {

        val adapter = ServicesAdapter(list!!,object : ServicesAdapter.OnItemClickListener
        {
            override fun onItemClick(item: MunicipalityModel) {
                try {
                    val url = item.webSite


                    val customTabsIntent = CustomTabsIntent.Builder()
                            .addDefaultShareMenuItem()
                            .setToolbarColor(this@MunicipalityFragment.resources.getColor(R.color.colorAccent))
                            .setShowTitle(true)
                            .build()
                    customTabsIntent.launchUrl(this@MunicipalityFragment.context,Uri.parse(url))


                }
                catch (e: Exception)
                {
                    Toast.makeText(context,"Web sayfasının tanımlanması ile ilgili bir sorun var.", Toast.LENGTH_LONG).show()

                }

            }


        }
        )

        rvHelpContact.adapter = adapter
        rvHelpContact.layoutManager = LinearLayoutManager(context)
    }

}
