package com.atum.bel.view.polls


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.atum.bel.R
import com.atum.bel.common.base.BaseFragment
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import kotlinx.android.synthetic.main.fragment_poll_list.*


class PollListFragment(val ahBottomNavigation: AHBottomNavigation) : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        init(ahBottomNavigation, activity!!.supportFragmentManager)
        return inflater.inflate(R.layout.fragment_poll_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imgPoll.setOnClickListener {
            openFragment(PollQuestFragment(ahBottomNavigation),PollQuestFragment::class.java.name)
        }
    }


}
