package com.atum.bel.view.projects;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.atum.bel.R;
import com.atum.bel.adapter.ProjectsSmallAdapter;
import com.atum.bel.api.ApiResultListener;
import com.atum.bel.common.ListType;
import com.atum.bel.common.base.BaseFragment;
import com.atum.bel.model.Project.ProjectListModel;
import com.atum.bel.model.Project.ProjectModel;
import com.atum.bel.model.Project.ProjectResponseModel;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;


public class ProducingAydinfragment extends BaseFragment {



    public ProducingAydinfragment() {
        // Required empty public constructor
    }

    private AHBottomNavigation bottomNavigationMenuView;

    public static ProducingAydinfragment newInstance(AHBottomNavigation bottomNavigationMenuView) {
        ProducingAydinfragment fragment = new ProducingAydinfragment();
        fragment.bottomNavigationMenuView = bottomNavigationMenuView;

        return fragment;
    }

    //View
    private View root;
    private TextView txtContinousProjects;
    private RecyclerView rvContinousProjects;
    private TextView txtInvestingProjects;
    private RecyclerView rvInvestingProjects;
    private Button btnFilter;
    private LinearLayout lnrFilter;
    private CheckBox cbInvestingProj;
    private CheckBox cbContinousProj;
    private CheckBox cbCompletedProj;
    private ImageButton ibtnSwitchList;
    private EditText etSearch;

    //Objects
    private List<ProjectModel> continousprojects;
    private List<ProjectModel> investingProjects;
    private Activity activity;
    private ListType selectedListType = ListType.Expanded;
    private ProjectListModel projectListModel;


void callService()
{
    ApiResultListener projectsApiListener = new ApiResultListener() {
        @Override
        public void succeed ( @NotNull String fromJson ) {

            Gson gson = new Gson();
            projectListModel = gson.fromJson(fromJson, ProjectResponseModel.class).getResult();
            migarateDatas();

        }

        @Override
        public void fail ( @org.jetbrains.annotations.Nullable String errorMessage ) {
            Toast.makeText(getContext(),errorMessage,Toast.LENGTH_LONG).show();

        }
    };

    ProjectApiClient client = new ProjectApiClient(this.getContext(),projectsApiListener);
    client.getProjects();

}

void searchService(String key)
{
    ApiResultListener projectSearchResultListener = new ApiResultListener() {
        @Override
        public void succeed ( @NotNull String fromJson ) {
            Gson gson = new Gson();
            projectListModel = gson.fromJson(fromJson, ProjectResponseModel.class).getResult();
            migarateDatas();
        }

        @Override
        public void fail ( @Nullable String errorMessage ) {
            Toast.makeText(getContext(),errorMessage,Toast.LENGTH_LONG).show();

        }
    };

    new ProjectApiClient(getContext(),projectSearchResultListener).sreachInProjects(key);
}

private void migarateDatas () {
    continousprojects = projectListModel.getContiuning();
    investingProjects = projectListModel.getInvestment();
    fillDatas();
}

@Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_producing_aydinfragment, container, false);

        activity = getActivity();

        init(bottomNavigationMenuView,((FragmentActivity) Objects.requireNonNull(activity)).getSupportFragmentManager());

        initView();

        callService();


        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction ( TextView v, int actionId, KeyEvent event ) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String key = etSearch.getText().toString();
                    if (!key.equalsIgnoreCase(""))
                    {
                        searchService(key);
                    }

                    return true;
                }
                return false;
            }
        });



        return root;
    }

    private void fillDatas() {
        ProjectsSmallAdapter continousProjectsSmallAdapter = new ProjectsSmallAdapter(continousprojects, selectedListType, new ProjectsSmallAdapter.OnItemClickListener() {
            @Override
            public void onItemClick ( ProjectModel project ) {

                openFragment(new ProjectDetailFragment(bottomNavigationMenuView, project), ProjectDetailFragment.class.getName());


            }
        });

        rvContinousProjects.setLayoutManager(new LinearLayoutManager(getContext()));
        rvContinousProjects.setAdapter(continousProjectsSmallAdapter);

        ProjectsSmallAdapter investingProjectsSmallAdapter = new ProjectsSmallAdapter(investingProjects, selectedListType, new ProjectsSmallAdapter.OnItemClickListener() {
            @Override
            public void onItemClick ( ProjectModel project ) {
                openFragment(new ProjectDetailFragment(bottomNavigationMenuView, project), ProjectDetailFragment.class.getName());

            }
        });

        rvInvestingProjects.setLayoutManager(new LinearLayoutManager(getContext()));

        rvInvestingProjects.setAdapter(investingProjectsSmallAdapter);


        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (lnrFilter.getVisibility() == View.VISIBLE)
                {
                    cbCompletedProj.setChecked(false);
                    cbContinousProj.setChecked(false);
                    cbInvestingProj.setChecked(false);

                    manageListSessionVisibility();

                    lnrFilter.setVisibility(View.GONE);
                    btnFilter.setTextColor(getResources().getColor(R.color.textcolorbtn));

                }else
                {
                    lnrFilter.setVisibility(View.VISIBLE);
                    btnFilter.setTextColor(getResources().getColor(R.color.olivegreen));


                }

            }
        });

        cbInvestingProj.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked)
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        cbContinousProj.setChecked(false);
                        cbCompletedProj.setChecked(false);
                    }
                });
                manageListSessionVisibility();



            }
        });

        cbCompletedProj.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked)
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cbContinousProj.setChecked(false);
                        cbInvestingProj.setChecked(false);
                    }
                });

                manageListSessionVisibility();

            }
        });

        cbContinousProj.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        cbCompletedProj.setChecked(false);
                        cbInvestingProj.setChecked(false);
                    }
                });

                manageListSessionVisibility();




            }
        });

        ibtnSwitchList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedListType.equals(ListType.Expanded))
                {
                    selectedListType = ListType.Small;
                    ibtnSwitchList.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_filter_selected));
                }
                else
                {
                    selectedListType = ListType.Expanded;
                    ibtnSwitchList.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_list_unselected));

                }

                migarateDatas();
            }
        });



    }

    private void manageListSessionVisibility() {


            txtInvestingProjects.setVisibility(cbInvestingProj.isChecked() ? View.VISIBLE : View.GONE );
            rvInvestingProjects.setVisibility(cbInvestingProj.isChecked() ? View.VISIBLE : View.GONE);

            txtContinousProjects.setVisibility(cbContinousProj.isChecked() ? View.VISIBLE : View.GONE);
            rvContinousProjects.setVisibility(cbContinousProj.isChecked() ? View.VISIBLE : View.GONE);



            if (!cbContinousProj.isChecked() && !cbCompletedProj.isChecked() && !cbInvestingProj.isChecked())
            {
                txtInvestingProjects.setVisibility(View.VISIBLE);
                rvInvestingProjects.setVisibility(View.VISIBLE);
                txtContinousProjects.setVisibility(View.VISIBLE);
                rvContinousProjects.setVisibility(View.VISIBLE);
            }


    }

    private void initView() {
        txtContinousProjects = root.findViewById(R.id.txtContinousProjects);
        rvContinousProjects = root.findViewById(R.id.rvContinousProjects);
        txtInvestingProjects = root.findViewById(R.id.txtInvestingProjects);
        rvInvestingProjects = root.findViewById(R.id.rvInvestingProjects);
        btnFilter = root.findViewById(R.id.btnFilter);
        lnrFilter = root.findViewById(R.id.lnrFilter);
        cbInvestingProj = root.findViewById(R.id.cbInvestingProj);
        cbContinousProj = root.findViewById(R.id.cbContinousProj);
        cbCompletedProj = root.findViewById(R.id.cbCompletedProj);
        ibtnSwitchList = root.findViewById(R.id.ibtnSwitchList);
        etSearch = root.findViewById(R.id.etSearch);

    }




}
