package com.atum.bel.view.projects

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.atum.bel.R
import com.atum.bel.common.ProjectType
import com.atum.bel.common.base.BaseFragment
import com.atum.bel.model.Project.ProjectModel
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_project_detail.*


class ProjectDetailFragment(val bottomNavigationView: AHBottomNavigation, val projectModel: ProjectModel) : BaseFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        init(bottomNavigationView, activity?.supportFragmentManager)
        return inflater.inflate(R.layout.fragment_project_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        txtProjName.text = projectModel.title

        txtProjDesc.text = projectModel.description

        Glide.with(this).load(projectModel.image_list.get(0)).into(imgDetail)

        writeProjectType(ProjectType.from(projectModel.proejct_type))


        ibtnBackPress.setOnClickListener {
            onBackPress()
        }

        ibtnGallery.setOnClickListener {
            val intent = Intent(this.activity,GallleryActivity::class.java)
            intent.putStringArrayListExtra("links",projectModel.image_list)
            startActivity(intent)
        }
    }

    private fun writeProjectType(type: ProjectType) {

        when (type)
        {
            ProjectType.Contiuning -> txtCategory.text = getString(R.string.devam_eden_projeler)
            ProjectType.Complated -> txtCategory.text = getString(R.string.tamamlanmis_porojeler)
            ProjectType.Investment -> txtCategory.text = getString(R.string.yatirim_projeleri)

        }

    }


}




