package com.atum.bel.view.events

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener

class EventsApiClient(context: Context, listener: ApiResultListener) : ApiClient(context, listener) {

    fun getAllEvents ( pagenumber:Int)
    {
        createGetRequest("event/all/".plus(pagenumber))
    }

    fun getClosedEvents(pagenumber: Int)
    {
        createGetRequest("event/close/".plus(pagenumber))
    }
}