package com.atum.bel.view.helpcontact

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener

class HelpContactApiClient(context: Context, listener: ApiResultListener) : ApiClient(context,listener)  {

    fun getContacts()
    {
        createGetRequest("help")
    }
}