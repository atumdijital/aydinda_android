package com.atum.bel.view.president


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.atum.bel.R
import com.atum.bel.adapter.PresidenSlider
import com.atum.bel.adapter.PresidentGalleryAdapter
import com.atum.bel.common.base.BaseFragment
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import kotlinx.android.synthetic.main.fragment_president_gallery.*

class PresidentGalleryFragment(val ahBottomNavigation: AHBottomNavigation)  : BaseFragment() {


    var dummylist: MutableList<Int> = generateDummies()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init(ahBottomNavigation,activity?.supportFragmentManager)
    }

    private fun generateDummies(): MutableList<Int> {

        val dummy = R.drawable.dummy1
        val dummy2 = R.drawable.dummy2
        val dummy3 = R.drawable.dummy3
        val dummy4 = R.drawable.dummy4
        val dummy5 = R.drawable.dummy5


        return listOf(dummy,dummy2,dummy3,dummy4,dummy5).toMutableList()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_president_gallery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = PresidenSlider(dummylist)

        president_slider_view.create(adapter, lifecycle = lifecycle)
        president_slider_view.isCycling = true


        rvImagesGrid.layoutManager = GridLayoutManager(context,3)
        rvImagesGrid.setHasFixedSize(true)
        rvImagesGrid.adapter = PresidentGalleryAdapter(dummylist,object : PresidentGalleryAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {
                president_slider_view.stopAutoCycle()
               val slider_pos =  president_slider_view.getCurrentPosition()
                val grid_pos = position

                if (slider_pos > grid_pos)
                {
                    val fark = slider_pos - grid_pos

                    for (x in 0 until fark)
                    {
                        president_slider_view.movePrevPosition(true)
                    }
                }
                else
                {
                    val fark = grid_pos - slider_pos
                    for (x in 0 until fark)
                    {
                        president_slider_view.moveNextPosition(true)
                    }

                }
            }
        })

    }






}
