package com.atum.bel.view.projects

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.atum.bel.R
import com.atum.bel.adapter.GalleryAdapter
import com.opensooq.pluto.PlutoView

class GallleryActivity : AppCompatActivity() {

    lateinit var links : ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_galllery)

        links = intent.getStringArrayListExtra("links")

        val pluto = findViewById<PlutoView>(R.id.slider_view)
        val adapter = GalleryAdapter(links)

        pluto.create(adapter, lifecycle = lifecycle)

    }


}
