package com.atum.bel.view.complaint

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener
import com.atum.bel.model.complaint.ComplaintModel


class ComplaintsApiClient( context: Context, listener: ApiResultListener ) : ApiClient(context, listener)
{

    fun postComplaint(compliant:ComplaintModel)
    {
        val params = HashMap<String, String>()
        params["name_surname"] = compliant.name_surname
        params["cap"] = compliant.cap
        params["explanation"] = compliant.explanation


        createPostRequest(params,"complaint")
    }
}