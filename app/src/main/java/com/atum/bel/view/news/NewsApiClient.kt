package com.atum.bel.view.news

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener

class NewsApiClient( context: Context, listener: ApiResultListener ): ApiClient(context, listener) {

    fun getNews()
    {
        createGetRequest("news")
    }

    fun search(key:String)
    {
        createGetRequest("news/".plus(key))
    }
}