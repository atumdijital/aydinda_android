package com.atum.bel.view.president

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.atum.bel.R
import com.atum.bel.common.base.BaseFragment
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import kotlinx.android.synthetic.main.fragment_ozlem_cerc.*


class OzlemCercFragment(val ahBottomNavigation : AHBottomNavigation) : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init(ahBottomNavigation, activity?.supportFragmentManager)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ozlem_cerc, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnAbout.setOnClickListener{
            openFragment(PresidentFragment(ahBottomNavigation), PresidentFragment::class.java.name)
        }

        btnGallery.setOnClickListener{

            openFragment(PresidentGalleryFragment(ahBottomNavigation), PresidentGalleryFragment::class.java.name)
        }

    }






}
