package com.atum.bel.view.auth

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener
import com.atum.bel.model.auth.Auth

class AuthApiClient (context: Context, listener: ApiResultListener) : ApiClient(context,listener)
{

    fun auth (auth: Auth)
    {
        val params = HashMap<String, String>()
        params["phone_number"] = auth.phone_number
        params["password"] = auth.password
        params["device_id"] = auth.device_id
        params["device_type"] = auth.device_type
        params["app_version"] = auth.app_version



        createPostRequest(params,"user/login")
    }
}