package com.atum.bel.view.complaint

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.atum.bel.R
import com.atum.bel.common.base.BaseFragment
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import kotlinx.android.synthetic.main.fragment_complaints.*

class ComplaintsFragment(val ahBottomNavigation: AHBottomNavigation) : BaseFragment() {



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        init(ahBottomNavigation, activity!!.supportFragmentManager)
        return inflater.inflate(R.layout.fragment_complaints, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtNewAppeal.setOnClickListener {
            openFragment(NewAppealFragment(ahBottomNavigation),NewAppealFragment::class.java.name)
        }

        btnBack.setOnClickListener {
            onBackPress()
        }
    }






}
