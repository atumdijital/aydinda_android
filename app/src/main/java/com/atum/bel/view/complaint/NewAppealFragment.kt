package com.atum.bel.view.complaint

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.atum.bel.R
import com.atum.bel.api.ApiResultListener
import com.atum.bel.common.base.BaseFragment
import com.atum.bel.model.complaint.ComplaintModel
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import kotlinx.android.synthetic.main.fragment_new_appeal.*


class NewAppealFragment(val ahBottomNavigation: AHBottomNavigation) : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        init(ahBottomNavigation, activity!!.supportFragmentManager)
        return inflater.inflate(R.layout.fragment_new_appeal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnBack.setOnClickListener {
            onBackPress()
        }
        btnSend.setOnClickListener {
            etDesc.isNotEmpty()
            etNameSurname.isNotEmpty()
            etTitle.isNotEmpty()

             val apiClient = object  :ApiResultListener {
                 override fun succeed(fromJson: String) {
                     Toast.makeText(context,"Bildiriminiz iletildi.",Toast.LENGTH_LONG).show()
                     onBackPress()

                 }

                 override fun fail(errorMessage: String?) {
                     Toast.makeText(context,errorMessage, Toast.LENGTH_LONG).show()

                 }

             }


            ComplaintsApiClient(context!!,apiClient).postComplaint(ComplaintModel(etNameSurname.text.toString(),etTitle.text.toString(),etDesc.text.toString()))

        }
    }


    fun EditText.isNotEmpty() : Unit
    {
        if (text.isNullOrBlank())
        {
            Toast.makeText(context,"Bütün alanları doldurmalısınız",Toast.LENGTH_LONG).show()
            return

        }
    }




}
