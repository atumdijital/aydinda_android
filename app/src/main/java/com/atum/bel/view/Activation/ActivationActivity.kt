package com.atum.bel.view.Activation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.atum.bel.R
import com.atum.bel.api.ApiResultListener
import com.atum.bel.view.auth.AuthActivity
import kotlinx.android.synthetic.main.activity_activation.*

class ActivationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activation)

        val listener = object : ApiResultListener{
            override fun succeed(fromJson: String) {
                startActivity(Intent(this@ActivationActivity,AuthActivity::class.java))
            }

            override fun fail(errorMessage: String?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }

        txtPhone.text =  intent.getStringExtra("num")

        btnBack.setOnClickListener {
            super.onBackPressed()
        }

        btnActivate.setOnClickListener {
            ActivationApiClient(this@ActivationActivity,listener).activate(txtPhone.text.toString(),appCompatEditText.text.toString())
        }
    }
}
