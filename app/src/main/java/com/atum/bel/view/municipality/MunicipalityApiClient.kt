package com.atum.bel.view.municipality

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener

class MunicipalityApiClient(context: Context, listener: ApiResultListener) : ApiClient(context,listener) {

    fun getServices()
    {
        createGetRequest("municipality")
    }
}