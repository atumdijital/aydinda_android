package com.atum.bel.view.main

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener

 class MainScreenApiClient (context: Context,listener:ApiResultListener) : ApiClient(context,listener) {


    fun getAnnounces()
    {
        createGetRequest("announcement")
    }

     fun getTabs()
     {
         createGetRequest("tab")
     }

}