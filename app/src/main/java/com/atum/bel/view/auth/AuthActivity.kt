package com.atum.bel.view.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.atum.bel.R
import com.atum.bel.api.ApiResultListener
import com.atum.bel.model.auth.Auth
import com.atum.bel.view.general.MainActivity
import com.atum.bel.view.login.LoginActivity
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)


        val listener = object : ApiResultListener{
            override fun succeed(fromJson: String) {
               startActivity( Intent(this@AuthActivity,MainActivity::class.java))

            }

            override fun fail(errorMessage: String?) {

                Toast.makeText(this@AuthActivity,errorMessage,Toast.LENGTH_LONG).show()

            }
        }

        btnLogin.setOnClickListener {
            val auth =Auth(etPhoneNumber.text.toString(),appCompatEditText.text.toString(),"qwueoıu-oıquweoı","A","1.0")
            AuthApiClient(this@AuthActivity,listener).auth(auth)
        }


        txtActivation.setOnClickListener {
            startActivity( Intent(this@AuthActivity,LoginActivity::class.java))

        }
    }
}
