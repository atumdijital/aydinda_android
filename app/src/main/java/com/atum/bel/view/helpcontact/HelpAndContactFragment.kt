package com.atum.bel.view.helpcontact

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.atum.bel.R
import com.atum.bel.adapter.HelpContactsAdapter
import com.atum.bel.api.ApiResultListener
import com.atum.bel.common.base.BaseFragment
import com.atum.bel.model.help.HelpModel
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_help_and_contact.*


class HelpAndContactFragment(val ahBottomNavigation: AHBottomNavigation) : BaseFragment() {

    private var list: MutableList<HelpModel>? = null
    lateinit var number : String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        init(ahBottomNavigation, activity!!.supportFragmentManager)
        return inflater.inflate(R.layout.fragment_help_and_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val apiClient = object : ApiResultListener{
            override fun succeed(fromJson: String) {

                val gson = Gson()

                list = gson.fromJson(fromJson, HelpModel.HelpResponseModel::class.java).result

                setAdapter()
            }

            override fun fail(errorMessage: String?) {
                Toast.makeText(context,errorMessage, Toast.LENGTH_LONG).show()
            }

        }


        HelpContactApiClient(context!!,apiClient).getContacts()

        btnBack.setOnClickListener { onBackPress() }


    }

    private fun setAdapter() {
        val adapter = HelpContactsAdapter(list!!,object : HelpContactsAdapter.onItemClickListener
        {
            override fun onItemClick(item: HelpModel) {
                number = item.phone
                if (ActivityCompat.checkSelfPermission(activity!!,
                                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                    phoneCall(number)
                }else {
                    val permissions = arrayOf(Manifest.permission.CALL_PHONE)
                    //Asking request Permissions
                    ActivityCompat.requestPermissions(activity!!, permissions, 9);
                }
            }


        })

        rvHelpContact.adapter = adapter
        rvHelpContact.layoutManager = LinearLayoutManager(context)
    }
    private fun phoneCall( number: String) {
        if (ActivityCompat.checkSelfPermission(activity!!,
                        Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse("tel:".plus(number))
            activity?.startActivity(callIntent)
        } else {
            Toast.makeText(activity, "You don't assign permission.", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        var permissionGranted = false
        when (requestCode) {
            9 -> permissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED
        }
        if (permissionGranted) {
            phoneCall(number)
        } else {
            Toast.makeText(activity, "You don't assign permission.", Toast.LENGTH_SHORT).show()
        }
    }


}
