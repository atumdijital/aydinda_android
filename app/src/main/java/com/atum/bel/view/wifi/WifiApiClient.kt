package com.atum.bel.view.wifi

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener

class WifiApiClient(context: Context, listener: ApiResultListener) : ApiClient(context, listener) {

    fun wifiPoints()
    {
        createGetRequest("wifi")
    }
}