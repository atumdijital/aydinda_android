package com.atum.bel.view.pharmacy

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener

class PharmacyApiClient( context: Context,
                         listener: ApiResultListener) : ApiClient(context,listener) {


    fun listPharmacies(): Unit {
        createGetRequest("pharmacy")

    }

}