package com.atum.bel.view.Activation

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener

class ActivationApiClient(context: Context, listener: ApiResultListener) : ApiClient(context,listener) {

    fun activate (number: String , code: String )
    {

        val params = HashMap<String, String>()
        params["phone_number"] = "90".plus(number)
        params["activation_code"] = code

        createPostRequest(params,"user/phoneValidation")
    }
}