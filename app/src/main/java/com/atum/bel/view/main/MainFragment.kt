package com.atum.bel.view.main

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.atum.bel.R
import com.atum.bel.adapter.MainBottomAdapter
import com.atum.bel.adapter.SliderAdapter
import com.atum.bel.api.ApiResultListener
import com.atum.bel.common.base.BaseFragment
import com.atum.bel.model.MainSlider.SliderModel
import com.atum.bel.model.MainSlider.SliderResponseModel
import com.atum.bel.model.Tab.TabModel
import com.atum.bel.model.Tab.TabResponseModel
import com.atum.bel.view.pharmacy.PharmacyFragment
import com.atum.bel.view.president.OzlemCercFragment
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_main.*
import java.util.*


class MainFragment(val ahBottomNavigation: AHBottomNavigation): BaseFragment() {


    private var viewpager: ViewPager? = null
    private var mView: View? = null
    private var indicator: TabLayout? = null
    private var rvBottomList: RecyclerView? = null

    private var list: MutableList<SliderModel>? = null
    private var tabList: MutableList<TabModel>? = null

    private var timer: Timer? = null

    private var sliderAdapter: SliderAdapter? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val announceApiCallListener = object : ApiResultListener {
            override fun succeed(fromJson: String) {


                Log.d("tag",fromJson)
                val gson =  Gson()

                val staff : SliderResponseModel = gson.fromJson(fromJson, SliderResponseModel::class.java)

                list = staff.result

                setAdapter()

            }

            override fun fail(errorMessage: String?) {

                Toast.makeText(context,errorMessage,Toast.LENGTH_LONG).show()
            }
        }



        MainScreenApiClient(context!!, announceApiCallListener).getAnnounces()

        val tabApiCallListener = object : ApiResultListener{
            override fun succeed(fromJson: String) {
                val gson = Gson()

                tabList = gson.fromJson(fromJson,TabResponseModel::class.java).result

                setBottomAdapter()

            }

            override fun fail(errorMessage: String?) {
                Toast.makeText(context,errorMessage,Toast.LENGTH_LONG).show()
            }
        }

        MainScreenApiClient(context!!, tabApiCallListener).getTabs()


    }

    private fun setBottomAdapter() {
        val linearLayoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        val mainBottomAdapter = MainBottomAdapter(MainBottomAdapter.onItemClickListener {
            openFragment(PharmacyFragment(ahBottomNavigation, activity!!.supportFragmentManager ),PharmacyFragment::class.java.name)
        }, tabList)

        rvBottomList!!.layoutManager = linearLayoutManager
        rvBottomList!!.adapter = mainBottomAdapter
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_main, container, false)

            initView()
            init(ahBottomNavigation, activity?.supportFragmentManager)


        }
        return mView
    }



    private fun setAdapter() {

        if (viewpager!!.adapter == null) {
            sliderAdapter = SliderAdapter(context!!, list!!)
            viewpager!!.adapter = sliderAdapter

        }

        viewpager!!.setCurrentItem(0, true)
        viewpager!!.offscreenPageLimit = 3
        indicator!!.setupWithViewPager(viewpager, true)

        if (timer == null) {
            timer = Timer()
            timer!!.scheduleAtFixedRate(SliderTimer(), 5000, 5000)
        }
    }

    private fun initView() {

        viewpager = mView!!.findViewById(R.id.viewpager)
        indicator = mView!!.findViewById(R.id.indicator)
        rvBottomList = mView!!.findViewById(R.id.rvBottomList)
    }

    private fun openDialogFragment()
    {




        val dialog = Dialog(context,android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        dialog.setContentView(R.layout.fragment_social_dialog)
        val d = ColorDrawable(Color.BLACK)
        d.alpha = 130
        dialog.window.setBackgroundDrawable(d)

        val ibtnSocialSolid = dialog.findViewById<ImageButton>(R.id.ibtnSocialSolid)
        val ibtnBaskan = dialog.findViewById<ImageButton>(R.id.ibtnBaskan)
        val ibtnFacebook = dialog.findViewById<ImageButton>(R.id.ibtnFacebook)
        val ibtnTwitter = dialog.findViewById<ImageButton>(R.id.ibtnTwitter)
        val anywhere = dialog.findViewById<ConstraintLayout>(R.id.anywhere)

        anywhere.setOnClickListener{
            dialog.dismiss()
        }
        ibtnSocialSolid.setOnClickListener {
            dialog.dismiss()
        }

        ibtnBaskan.setOnClickListener {

            dialog.dismiss()
            openFragment(OzlemCercFragment(ahBottomNavigation), OzlemCercFragment::class.java.name)
        }

        ibtnFacebook.setOnClickListener {
            dialog.dismiss()

            val url = "https://tr-tr.facebook.com/BsknOzlemCercioglu/"

            val i =  Intent(Intent.ACTION_VIEW)
            i.setData(Uri.parse(url))
            startActivity(i)
        }

        ibtnTwitter.setOnClickListener {
            dialog.dismiss()

            val url = "https://twitter.com/OZLEM_CERCIOGLU"

                    val i =  Intent(Intent.ACTION_VIEW)
                    i.setData(Uri.parse(url))
                    startActivity(i)
        }



        dialog.show()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ibtnSocial.setOnClickListener {
            openDialogFragment()
        }
    }

    private inner class SliderTimer : TimerTask() {

        override fun run() {
            Objects.requireNonNull<FragmentActivity>(activity).runOnUiThread(Runnable {
                if (viewpager!!.currentItem < list!!.size - 1) {
                    viewpager!!.currentItem = viewpager!!.currentItem + 1
                } else {
                    viewpager!!.currentItem = 0
                }
            })
        }
    }

}


