package com.atum.bel.view.VoyageDetail

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener

class VoyageDetailApiClient(context: Context,listener: ApiResultListener) : ApiClient(context, listener) {

    fun getDetails(key:String)
    {
        createGetRequest("route/detail/".plus(key))
    }

    fun getRouteAnnounces (id:String)
    {
        createGetRequest("route/announcement/".plus(id))
    }
}