package com.atum.bel.view.wifi


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.atum.bel.R
import com.atum.bel.adapter.WifiPagerAdapter
import com.atum.bel.common.base.BaseFragment
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import kotlinx.android.synthetic.main.fragment_wifi.*




class WifiFragment(val ahBottomNavigation: AHBottomNavigation) : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        init(ahBottomNavigation,activity?.supportFragmentManager)
        return inflater.inflate(R.layout.fragment_wifi, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vpWifiPages.adapter = WifiPagerAdapter(childFragmentManager)

        vpWifiPages.currentItem = 0

        btnWifiPoints.setOnClickListener {
            vpWifiPages.currentItem = 0
        }

        btnWifiServiceInfo.setOnClickListener {

            vpWifiPages.currentItem = 1
        }

        vpWifiPages.addOnPageChangeListener(object : OnPageChangeListener {

            // This method will be invoked when a new page becomes selected.
            override fun onPageSelected(position: Int) {

                when(position)
                {
                    0 -> {

                        makeButtonSelected(btnWifiPoints)
                        makeButtonUnselected(btnWifiServiceInfo)

                    }
                    1 ->
                    {
                        makeButtonSelected(btnWifiServiceInfo)
                        makeButtonUnselected(btnWifiPoints)
                    }

                }

            }

            // This method will be invoked when the current page is scrolled
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            override fun onPageScrollStateChanged(state: Int) {

            }
        }




        )


    }

    private fun makeButtonUnselected(button: Button?) {
        button?.setTextColor(resources.getColor(R.color.olivegreen))
        button?.setBackgroundColor( resources.getColor(android.R.color.white))
    }

    private fun makeButtonSelected(button: Button?) {
        button?.setTextColor(resources.getColor(android.R.color.white))
        button?.setBackgroundColor( resources.getColor(R.color.olivegreen))
    }

}
