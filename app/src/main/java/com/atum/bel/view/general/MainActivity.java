package com.atum.bel.view.general;

import android.os.Bundle;

import androidx.core.content.ContextCompat;

import com.atum.bel.R;
import com.atum.bel.common.base.BaseActivity;
import com.atum.bel.view.Other.OtherFragment;
import com.atum.bel.view.main.MainFragment;
import com.atum.bel.view.projects.ProducingAydinfragment;
import com.atum.bel.view.voyage.VoyagesFragment;
import com.atum.bel.view.wifi.WifiFragment;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {

    private AHBottomNavigation ahBottomNavigation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

    }

    private void initView() {
        ahBottomNavigation = findViewById(R.id.ahBottomNavigation);
        init(ahBottomNavigation,getSupportFragmentManager());
        initBottomNaviItems();

    }

    private void initBottomNaviItems() {
        // Create items


        List<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();

        AHBottomNavigationItem mainItem = new AHBottomNavigationItem(getString(R.string.gunaydın), R.drawable.ic_bottom_main);
        AHBottomNavigationItem voyagesItem = new AHBottomNavigationItem(getString(R.string.seferler), R.drawable.ic_voyages);
        AHBottomNavigationItem productionItem = new AHBottomNavigationItem(getString(R.string.uretenaydin), R.drawable.ic_production);
        AHBottomNavigationItem wifiItem = new AHBottomNavigationItem(getString(R.string.wifi), R.drawable.ic_wifi);
        AHBottomNavigationItem otherItem = new AHBottomNavigationItem("Diğer",R.drawable.ic_other);


        bottomNavigationItems.add(mainItem);
        bottomNavigationItems.add(voyagesItem);
        bottomNavigationItems.add(productionItem);
        bottomNavigationItems.add(wifiItem);
        bottomNavigationItems.add(otherItem);

        ahBottomNavigation.addItems(bottomNavigationItems);

        ahBottomNavigation.enableItemAtPosition(0);
        ahBottomNavigation.enableItemAtPosition(1);
        ahBottomNavigation.enableItemAtPosition(2);
        ahBottomNavigation.enableItemAtPosition(3);
        ahBottomNavigation.enableItemAtPosition(4);


        ahBottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.bottomnavi1));
        ahBottomNavigation.setAccentColor(getResources().getColor(R.color.olivegreen));

        ahBottomNavigation.setActivated(true);
        ahBottomNavigation.setInactiveColor(ContextCompat.getColor(this, R.color.deselected_indicator));


        ahBottomNavigation.setBehaviorTranslationEnabled(false);

        ahBottomNavigation.setForceTint(true);



        ahBottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        ahBottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {

                    switch (position)
                    {
                        case 0:
                            openFragment(new MainFragment(ahBottomNavigation),MainFragment.class.getName());

                            break;

                        case 1:

                            openFragment( new VoyagesFragment(ahBottomNavigation),VoyagesFragment.class.getName());
                            break;


                        case 2:
                            openFragment(ProducingAydinfragment.newInstance(ahBottomNavigation),ProducingAydinfragment.class.getName());
                            break;

                        case 3:
                            openFragment(new WifiFragment(ahBottomNavigation),WifiFragment.class.getName());
                            break;
                        case 4:
                            openFragment(new OtherFragment(ahBottomNavigation),OtherFragment.class.getName());
                    }

                    ahBottomNavigation.refresh();
                    ahBottomNavigation.restoreBottomNavigation(true);





                return true;
            }
        });

        ahBottomNavigation.setCurrentItem(0,true);
        

        openFragment(new MainFragment(ahBottomNavigation),MainFragment.class.getName());


    }

    @Override
    public void onBackPressed() {

        onBackPress();
    }




    @Override
    public void onBackPress() {

        super.onBackPress();

        if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof  MainFragment)
        {
            ahBottomNavigation.setBackgroundColor(getResources().getColor(R.color.bottomnavi1));
        }
        else
        {
            ahBottomNavigation.setBackgroundColor(getResources().getColor(android.R.color.white));

        }
    }
}
