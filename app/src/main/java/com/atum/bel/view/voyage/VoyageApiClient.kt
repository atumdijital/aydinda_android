package com.atum.bel.view.voyage

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener

class VoyageApiClient(context: Context, listener: ApiResultListener) : ApiClient(context,listener) {

    fun listRoutes()
    {
        createGetRequest("route")
    }

    fun searchRoutes(key : String)
    {
        createGetRequest("route/" +key)
    }

}