package com.atum.bel.view.news


import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Slide
import androidx.transition.TransitionManager
import com.atum.bel.R
import com.atum.bel.adapter.NewsAdapter
import com.atum.bel.api.ApiResultListener
import com.atum.bel.common.VisibleToggleClickListener
import com.atum.bel.common.base.BaseFragment
import com.atum.bel.model.news.NewsModel
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_news.*


class NewsFragment(val ahBottomNavigation: AHBottomNavigation) : BaseFragment() {

    private var list: MutableList<NewsModel>? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        init(ahBottomNavigation,activity!!.supportFragmentManager)
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnBack.setOnClickListener {
            onBackPress()
        }

        container.findViewById<ImageButton>(R.id.ibSearch).setOnClickListener(object : VisibleToggleClickListener() {
            override fun changeVisibility(visible: Boolean) {
                TransitionManager.beginDelayedTransition(container, Slide(Gravity.START))
                textView12.setVisibility(if (visible) View.VISIBLE else View.GONE)
                ibSearch.setVisibility(if (visible) View.VISIBLE else View.GONE)
                etNewsSearch.setVisibility(if (visible) View.GONE else View.VISIBLE)
            }
        })

        etNewsSearch?.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!v.text.isNullOrBlank())
                        search(v.text.toString())
                    return true
                }
                return false
            }
        })








        val client = object : ApiResultListener{
            override fun succeed(fromJson: String) {
                val gson =  Gson()
                val result : MutableList<NewsModel>? = gson.fromJson(fromJson, NewsModel.NewsResponseModel::class.java).result
                list = result

                setAdapter()
            }

            override fun fail(errorMessage: String?) {
                Toast.makeText(context,errorMessage, Toast.LENGTH_LONG).show()
            }

        }

        NewsApiClient(context!!,client).getNews()


    }

    private fun search(text: String) {

        val client = object : ApiResultListener{
            override fun succeed(fromJson: String) {
                val gson =  Gson()
                val result : MutableList<NewsModel>? = gson.fromJson(fromJson, NewsModel.NewsResponseModel::class.java).result!!


                    list = result

                    setAdapter()


            }

            override fun fail(errorMessage: String?) {
                Toast.makeText(context,errorMessage, Toast.LENGTH_LONG).show()
            }

        }

        NewsApiClient(context!!,client).search(text)


    }

    private fun setAdapter() {

        try {
            val adapter = NewsAdapter(list!!, object :  NewsAdapter.NewsItemOnClickListener{
                override fun onClick(it: NewsModel) {
                    openFragment(NewsDetailFragment(ahBottomNavigation,it),NewsDetailFragment::class.java.name)
                }

            })

            rvNews.adapter = adapter
            rvNews.layoutManager = LinearLayoutManager(context)
        }
        catch (e:Exception)
        {
            Toast.makeText(context,e.message, Toast.LENGTH_LONG).show()

        }


    }


}
