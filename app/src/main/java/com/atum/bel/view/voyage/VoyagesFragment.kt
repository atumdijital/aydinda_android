package com.atum.bel.view.voyage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.atum.bel.R
import com.atum.bel.adapter.VoyagesAdapter
import com.atum.bel.api.ApiResultListener
import com.atum.bel.common.base.BaseFragment
import com.atum.bel.model.Voyage.VoyageModel
import com.atum.bel.model.Voyage.VoyageResponseModel
import com.atum.bel.view.VoyageDetail.VoyageDetailsFragment
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_voyages.*


class VoyagesFragment(val ahBottomNavigation: AHBottomNavigation) : BaseFragment() {

    lateinit var routes : MutableList<VoyageModel>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        init(ahBottomNavigation, activity!!.supportFragmentManager)

        return inflater.inflate(R.layout.fragment_voyages, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getData()



        etSearchVoyage.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val key = etSearchVoyage.text.toString()
                if (!key.equals("", ignoreCase = true)) {
                    searchService(key)
                }

                return@OnEditorActionListener true
            }
            false
        })
    }

    private fun searchService(key: String) {

        val projectSearchResultListener = object : ApiResultListener {
            override fun succeed(fromJson: String) {
                val gson = Gson()
                routes = gson.fromJson<VoyageResponseModel>(fromJson, VoyageResponseModel::class.java!!).getResult()
                showResult()
            }

            override fun fail(errorMessage: String?) {
                Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()

            }
        }

        VoyageApiClient(context!!, projectSearchResultListener).searchRoutes(key)
    }

    private fun showResult() {
        rvVoyages.layoutManager = LinearLayoutManager(activity)

        rvVoyages.adapter = VoyagesAdapter(routes, object : VoyagesAdapter.OnItemClickListener {

            override fun onItemClick(obje: VoyageModel) {
                openFragment(VoyageDetailsFragment(ahBottomNavigation, obje), VoyageDetailsFragment::class.java.name)
            }

        })    }


    private fun getData() {
        val listener = object : ApiResultListener {
            override fun succeed(fromJson: String) {
                val gson = Gson()
                routes = gson.fromJson(fromJson, VoyageResponseModel::class.java).result
                showResult()

            }

            override fun fail(errorMessage: String?) {
                Toast.makeText(context, "Otobüs hatları getirilemedi.", Toast.LENGTH_LONG).show()
            }


        }
        VoyageApiClient(context!!,listener).listRoutes()
    }
}
