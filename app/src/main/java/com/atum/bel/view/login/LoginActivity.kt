package com.atum.bel.view.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.atum.bel.R
import com.atum.bel.api.ApiResultListener
import com.atum.bel.view.Activation.ActivationActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val listener = object : ApiResultListener{
            override fun succeed(fromJson: String) {
                val intent = Intent(this@LoginActivity,ActivationActivity::class.java)
                intent.putExtra("num",etPhone.text.toString())
                startActivity(intent)


            }

            override fun fail(errorMessage: String?) {
                Toast.makeText(this@LoginActivity,errorMessage,Toast.LENGTH_LONG).show()
            }

        }

        btnRegister.setOnClickListener {
            fireRequest(listener)
        }



    }

    private fun fireRequest(listener: ApiResultListener) {
        LoginApiClient(this@LoginActivity,listener ).register(etPhone.text.toString())
    }
}
