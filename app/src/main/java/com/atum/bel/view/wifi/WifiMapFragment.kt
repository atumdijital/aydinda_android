package com.atum.bel.view.wifi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.atum.bel.R
import com.atum.bel.api.ApiResultListener
import com.atum.bel.model.Wifi.WifiModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_wifi_map.*


class WifiMapFragment : Fragment() {
//    var mMapView: MapView? = null
    private var googleMap: GoogleMap? = null
    lateinit var points : MutableList<WifiModel>


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val listener = object :ApiResultListener{
            override fun succeed(fromJson: String) {
                val gson = Gson()
                points = gson.fromJson(fromJson, WifiModel.WifiResponseModel::class.java).result
                buildMapPoints()

            }

            override fun fail(errorMessage: String?) {
                Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
            }
        }

        WifiApiClient(context!!,listener).wifiPoints()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_wifi_map, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mapView.onCreate(savedInstanceState)

        mapView.onResume() // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(activity!!.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun buildMapPoints() {
        mapView.getMapAsync(OnMapReadyCallback { mMap ->
            googleMap = mMap

            // For showing a move to my location button
//            googleMap.setMyLocationEnabled(true)

            // For dropping a marker at a point on the Map

            for (point in points)
            {
                googleMap!!.addMarker(MarkerOptions().
                        position(LatLng(point.location.get(0),point.location.get(1)))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_wifi_map))
                ).tag = point.id

            }

            googleMap!!.setOnMarkerClickListener(GoogleMap.OnMarkerClickListener {

                for (x in points)
                {
                    if ( it.tag!!.equals(x.id))
                    {
                        openDialogFragment(x)
                        break

                    }
                }


                return@OnMarkerClickListener false


            }

            )

            // For zooming automatically to the location of the marker
            val cameraPosition = CameraPosition.Builder().target(LatLng(points.get(1).location.get(0),points.get(0).location.get(1))).zoom(12f).build()
            googleMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        })
    }

    private fun openDialogFragment( point:WifiModel)
    {

        val ft = fragmentManager!!.beginTransaction()
        val prev = fragmentManager!!.findFragmentByTag("dialog")
        if (prev != null) {
            ft.remove(prev)
        }
        ft.addToBackStack(null)
        val dialogFragment = MapDialog(point)
        dialogFragment.show(ft, "dialog")

    }


}
