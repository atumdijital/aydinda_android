package com.atum.bel.view.news


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.atum.bel.R
import com.atum.bel.common.DateUtility
import com.atum.bel.common.base.BaseFragment
import com.atum.bel.model.news.NewsModel
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_news_detail.*


class NewsDetailFragment(val ahBottomNavigation: AHBottomNavigation,  val new : NewsModel) : BaseFragment() {
    val datePattern = "dd.MM.yyyy HH:mm"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        init(ahBottomNavigation, activity!!.supportFragmentManager)
        return inflater.inflate(R.layout.fragment_news_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        txtNewsTitleDetail.text = new.title
        txtTimeDetail.text = DateUtility().getDateTimeFromEpocLongOfSeconds(new.create_time,datePattern)
        Glide.with(this).load(new.image_list.get(0)).into(ivNewsDetail)
        txtImageNewsDetail.text = new.desc

        btnBack.setOnClickListener {
            onBackPress()
        }
    }


}
