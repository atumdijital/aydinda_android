package com.atum.bel.view.login

import android.content.Context
import com.atum.bel.api.ApiClient
import com.atum.bel.api.ApiResultListener

class LoginApiClient(context: Context, listener: ApiResultListener) : ApiClient(context,listener)
{

    fun register(phonenumber:String)
    {
        val params = HashMap<String, String>()
        params["phone_number"] = "90".plus(phonenumber)
        createPostRequest(params,"user/register/phone")
    }
}