package com.atum.bel.view.Other

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.atum.bel.R
import com.atum.bel.common.base.BaseFragment
import com.atum.bel.view.complaint.ComplaintsFragment
import com.atum.bel.view.events.EventsFragment
import com.atum.bel.view.helpcontact.HelpAndContactFragment
import com.atum.bel.view.municipality.MunicipalityFragment
import com.atum.bel.view.news.NewsFragment
import com.atum.bel.view.polls.PollListFragment
import com.atum.bel.view.settings.SettingsFragment
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import kotlinx.android.synthetic.main.fragment_other.*


class OtherFragment(val ahBottomNavigation : AHBottomNavigation) : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        init(ahBottomNavigation, activity?.supportFragmentManager)
        return inflater.inflate(R.layout.fragment_other, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivMunicipality.setOnClickListener {
            openFragment(MunicipalityFragment(ahBottomNavigation),MunicipalityFragment::class.java.name)
        }

        ivHelpContacts.setOnClickListener {
            openFragment(HelpAndContactFragment(ahBottomNavigation),HelpAndContactFragment::class.java.name)
        }

        ivComplient.setOnClickListener {
            openFragment(ComplaintsFragment(ahBottomNavigation),ComplaintsFragment::class.java.name)
        }
        ivNews.setOnClickListener {
            openFragment(NewsFragment(ahBottomNavigation),NewsFragment::class.java.name)

        }

        ivEvents.setOnClickListener {
            openFragment(EventsFragment(ahBottomNavigation),EventsFragment::class.java.name)
        }

        ivSettings.setOnClickListener {
            openFragment(SettingsFragment(ahBottomNavigation),SettingsFragment::class.java.name)

        }
        ivPolls.setOnClickListener {
            openFragment(PollListFragment(ahBottomNavigation),PollListFragment::class.java.name)

        }
    }



}
