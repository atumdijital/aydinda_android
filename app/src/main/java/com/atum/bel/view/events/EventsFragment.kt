package com.atum.bel.view.events


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.atum.bel.R
import com.atum.bel.adapter.EventsAdapter
import com.atum.bel.api.ApiResultListener
import com.atum.bel.common.base.BaseFragment
import com.atum.bel.model.EventModel
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_events.*

class EventsFragment(val ahBottomNavigation: AHBottomNavigation) : BaseFragment() {
    var page = 0
    lateinit var list: MutableList<EventModel>
    lateinit var linearLayoutManager:LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        init(ahBottomNavigation, activity!!.supportFragmentManager)
        return inflater.inflate(R.layout.fragment_events, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getEvents(0)
        tlEvents.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }


            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }


            override fun onTabSelected(tab: TabLayout.Tab?) {
                page = 0;
                getEvents(tab!!.position)

            }
        })


    }

    private fun getEvents(position: Int) {

        val apiClient = object : ApiResultListener {
            override fun succeed(fromJson: String) {
                val gson = Gson()


                linearLayoutManager = LinearLayoutManager(context, VERTICAL ,false)
                rvEvents.layoutManager = linearLayoutManager
                if (page == 0)
                {
                    list = gson.fromJson(fromJson, EventModel.EventResponseModel::class.java).result
                    rvEvents.adapter = EventsAdapter(list)

                }
                else
                {
                    val newdatas = gson.fromJson(fromJson, EventModel.EventResponseModel::class.java).result
                    list.addAll(newdatas)
                    rvEvents.adapter!!.notifyItemRangeInserted(list.size - newdatas.size,newdatas.size)
                }

            }

            override fun fail(errorMessage: String?) {
                Toast.makeText(context,"Etkinlik bulunmamaktadır", Toast.LENGTH_LONG).show()

                list.clear()
                rvEvents.adapter = null

            }


        }
        if (position == 0)
            EventsApiClient(context!!,apiClient).getAllEvents(page)
        else if (position == 1)
            EventsApiClient(context!!,apiClient).getClosedEvents(page)

        rvEvents.addOnScrollListener(object : OnScrollListener(){

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                 val lastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition()
                val totalItemCount = rvEvents!!.layoutManager?.itemCount

                if (totalItemCount == lastVisibleItemPosition + 3)
                {
                    page ++
                    if (position == 0)
                        EventsApiClient(context!!,apiClient).getAllEvents(page)
                    else if (position == 1)
                        EventsApiClient(context!!,apiClient).getClosedEvents(page)



                }
            }

        })



    }


}
