package com.atum.bel.view.VoyageDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.atum.bel.R
import com.atum.bel.adapter.RouteAnnounceSliderAdapter
import com.atum.bel.adapter.VoyageHourAdapter
import com.atum.bel.api.ApiResultListener
import com.atum.bel.common.DateUtility
import com.atum.bel.common.base.BaseFragment
import com.atum.bel.model.RouteDetail.RouteCalendarModel
import com.atum.bel.model.RouteDetail.RouteDetailModel
import com.atum.bel.model.RouteDetail.RouteDetailResponseModel
import com.atum.bel.model.RouteDetail.RouteLineModel
import com.atum.bel.model.Voyage.VoyageModel
import com.atum.bel.model.VoyageAnnounce.VoyageAnnounceModel
import com.atum.bel.model.VoyageAnnounce.VoyageAnnounceResponseModel
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_voyage_details.*


class VoyageDetailsFragment (val ahBottomNavigation: AHBottomNavigation, val voyage: VoyageModel) : BaseFragment() {

    lateinit var detaildeRoute: RouteDetailModel
    var  isReturning = false
    var dayset = DateUtility().getCurrentDay()
    lateinit var routeAnnounces : MutableList<VoyageAnnounceModel>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        init(ahBottomNavigation, activity?.supportFragmentManager)
        return inflater.inflate(R.layout.fragment_voyage_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtLineNumber.text = voyage.lineNumber.toString()
        txtFrom.text = voyage.routeReturn
        txtTo.text = voyage.routeGoing

        callRouteDeatilHours()

        btnBack.setOnClickListener {
            onBackPress()
        }

        btnSaturday.setOnClickListener {

            showResult(DateUtility.CurrentDay.saturday)
        }

        btnWorkDays.setOnClickListener {
            showResult(DateUtility.CurrentDay.weekday)

        }

        btnSunday.setOnClickListener {
            showResult(DateUtility.CurrentDay.sunday)

        }

        btnRevert.setOnClickListener {
            isReturning = !isReturning
            if (!isReturning)
            {
                txtFrom.text = voyage.routeGoing
                txtTo.text = voyage.routeReturn
            }
            else
            {
                txtFrom.text = voyage.routeReturn
                txtTo.text = voyage.routeGoing
            }


            showResult(dayset)
        }

        callAnnouncesOfThisLine()

    }

    private fun callAnnouncesOfThisLine() {

        val listener: ApiResultListener = object : ApiResultListener
        {
            override fun succeed(fromJson: String) {
                val gson = Gson()
                routeAnnounces = gson.fromJson(fromJson,VoyageAnnounceResponseModel::class.java).result

                if (routeAnnounces.size > 0)
                setRouteAnnounces()
                else
                    vpAnnouncesLine.visibility =View.GONE

            }

            override fun fail(errorMessage: String?) {
                Toast.makeText(context, "Duyuru bildiğine ulaşılamadı.", Toast.LENGTH_LONG).show()
            }
        }

        VoyageDetailApiClient(context!!,listener).getRouteAnnounces(voyage.id)
    }

    private fun setRouteAnnounces() {
//        val pluto = findViewById<PlutoView>(R.id.slider_view)
        val adapter = RouteAnnounceSliderAdapter(routeAnnounces)

        vpAnnouncesLine.create(adapter, lifecycle = lifecycle)
    }

    private fun callRouteDeatilHours()
    {
          val listener = object : ApiResultListener {
              override fun succeed(fromJson: String) {
                  val gson = Gson()
                  detaildeRoute = gson.fromJson(fromJson, RouteDetailResponseModel::class.java).result
                  showResult(dayset)

              }

              override fun fail(errorMessage: String?) {
                  Toast.makeText(context, "Detaylar getirilemiyor.", Toast.LENGTH_LONG).show()
              }


          }
        VoyageDetailApiClient(context!!, listener).getDetails(voyage.id)
    }

    private fun showResult(selecteddayset: DateUtility.CurrentDay)
    {
         var calendarModel : RouteCalendarModel
        var hourList : List<List<RouteLineModel>> = detaildeRoute.routeGoingTime.weekday

        if (isReturning)
        {
            calendarModel = detaildeRoute.routeReturnTime
        }
        else
        {
            calendarModel = detaildeRoute.routeGoingTime
        }

        if (selecteddayset.equals(DateUtility.CurrentDay.weekday))
        {
            hourList = calendarModel.weekday
            activateWorkDaysButton()
        }else if (selecteddayset.equals( DateUtility.CurrentDay.saturday))
        {
            hourList = calendarModel.saturday
            activateSaturdayButton()
        }
        else if (selecteddayset.equals(DateUtility.CurrentDay.sunday))
        {
            hourList = calendarModel.sunday
            activateSundayButton()
        }

        setupAdapter(hourList)
    }

   fun setupAdapter( hours :List<List<RouteLineModel>>)
    {
        rvHourArea.layoutManager = LinearLayoutManager(context)
        rvHourArea.adapter = VoyageHourAdapter(context!!,hours)

    }


    private fun activateWorkDaysButton() {
        activeButton(btnWorkDays)
        deactiveButton(btnSaturday)
        deactiveButton(btnSunday)

    }

    private fun activateSundayButton() {
        activeButton(btnSunday)
        deactiveButton(btnWorkDays)
        deactiveButton(btnSaturday)
    }

    private fun activateSaturdayButton() {
        activeButton(btnSaturday)
        deactiveButton(btnWorkDays)
        deactiveButton(btnSunday)

    }

    private fun deactiveButton(button: Button?) {

        button?.setBackgroundColor(resources.getColor(R.color.unselected))
        button?.setTextColor(resources.getColor(R.color.textcolorbtn))


    }

    private fun activeButton(button: Button?) {
        button?.setBackgroundColor(resources.getColor(R.color.olivegreen))
        button?.setTextColor(resources.getColor(android.R.color.white))


    }


}
