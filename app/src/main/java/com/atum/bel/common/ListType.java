package com.atum.bel.common;

import android.util.SparseArray;

public enum ListType {


    Small(0),
    Expanded(1);

    public final Integer Value;

    private static final SparseArray<ListType> _map = new SparseArray<>();

    static {
        for (ListType data : ListType.values())
            _map.put(data.Value, data);
    }


    ListType(int value) {
        Value = value;
    }

    public static ListType from(int value) {
        return _map.get(value);
    }
}
