package com.atum.bel.common

import android.util.SparseArray

enum class ProjectType private constructor(val value: Int) {

    Contiuning(1),
    Complated(0),
    Investment(2);


    companion object {
        private val _map = SparseArray<ProjectType>()


        init {
            for (data in ProjectType.values())
                _map.put(data.value, data)
        }

        fun from(value: Int): ProjectType {
            return _map.get(value)
        }
    }
}
