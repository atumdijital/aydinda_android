package com.atum.bel.common

import android.annotation.SuppressLint
import android.util.SparseArray
import java.text.SimpleDateFormat
import java.util.*

class DateUtility {



    public fun getCurrentDay() : DateUtility.CurrentDay
    {
        val calendar = Calendar.getInstance(Locale("tr","tr"))
        val day = calendar.get(Calendar.DAY_OF_WEEK)

        when (day) {
            Calendar.SUNDAY -> {
                return CurrentDay.sunday
            }
            Calendar.SATURDAY -> {

                return CurrentDay.saturday
            }
            else ->
            {
                return CurrentDay.weekday
            }

        }
    }



    @SuppressLint("SimpleDateFormat")
    public fun getDateTimeFromEpocLongOfSeconds(epoc: Long, pattern: String): String? {
        try {
            val netDate = Date(epoc*1000)
            val formatter = SimpleDateFormat (pattern)
            return formatter.format(netDate).toString()
        } catch (e: Exception) {
            return e.toString()
        }
    }


    enum class CurrentDay private constructor(val value: Int)
    {

        weekday(0),
        saturday(1),
        sunday(2);


        companion object {
            private val _map = SparseArray<CurrentDay>()


            init {
                for (data in CurrentDay.values())
                    _map.put(data.value, data)
            }

            fun from(value: Int): CurrentDay {
                return _map.get(value)
            }
        }

    }




}