package com.atum.bel.common.base;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.atum.bel.view.main.MainFragment;
import com.atum.bel.R;
import com.atum.bel.view.president.OzlemCercFragment;
import com.atum.bel.view.president.PresidentFragment;
import com.atum.bel.view.president.PresidentGalleryFragment;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;

import java.util.Objects;

public abstract class BaseFragment extends Fragment implements ChangingFragment {

    private static FragmentManager supporFragmentManager;
    private static AHBottomNavigation ahBottomNavigation;



    @Override
    public void init(AHBottomNavigation ahBottomNavigation, FragmentManager supporFragmentManager) {

        BaseFragment.supporFragmentManager = supporFragmentManager;
        BaseFragment.ahBottomNavigation = ahBottomNavigation;

    }

    @Override
    public void openFragment(Fragment targetFragment, String tag) {

        FragmentTransaction transaction = supporFragmentManager.beginTransaction();
        Fragment newFragment = supporFragmentManager.findFragmentByTag(tag);

        if (targetFragment instanceof MainFragment)
            ahBottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.bottomnavi1));
        else
        {
            ahBottomNavigation.setDefaultBackgroundColor(getResources().getColor(android.R.color.white));
        }

        if (targetFragment instanceof OzlemCercFragment || targetFragment instanceof PresidentFragment || targetFragment instanceof PresidentGalleryFragment)
        {
            ahBottomNavigation.hideBottomNavigation(true);
        }
        else {
            ahBottomNavigation.restoreBottomNavigation(true);
        }



        if (newFragment == null) {
            transaction.add(R.id.container, targetFragment, targetFragment.getClass().getName());
            transaction.addToBackStack(targetFragment.getClass().getName());
            transaction.replace(R.id.container, targetFragment);
        } else {
            transaction.addToBackStack(targetFragment.getClass().getName());
            newFragment.setArguments(targetFragment.getArguments());
            transaction.replace(R.id.container, newFragment);



        }

        transaction.commitAllowingStateLoss();

    }

    @Override
    public void onBackPress() {

        Objects.requireNonNull(getActivity()).onBackPressed();

    }
}
