package com.atum.bel.common.base

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.atum.bel.R
import com.atum.bel.view.main.MainFragment
import com.atum.bel.view.president.OzlemCercFragment
import com.atum.bel.view.president.PresidentFragment
import com.atum.bel.view.president.PresidentGalleryFragment
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation

abstract class BaseActivity : AppCompatActivity(), ChangingFragment {


    override fun init(ahBottomNavigation: AHBottomNavigation, supporFragmentManager: FragmentManager) {
        BaseActivity.supporFragmentManager = supporFragmentManager
        BaseActivity.ahBottomNavigation = ahBottomNavigation
    }

    override fun openFragment(targetFragment: Fragment, tag: String) {
        val transaction = supporFragmentManager!!.beginTransaction()
        val newFragment = supporFragmentManager!!.findFragmentByTag(tag)

        if (targetFragment is MainFragment)
            ahBottomNavigation!!.defaultBackgroundColor = resources.getColor(R.color.bottomnavi1)
        else {
            ahBottomNavigation!!.defaultBackgroundColor = resources.getColor(android.R.color.white)
        }
        ahBottomNavigation!!.restoreBottomNavigation(true)



        if (newFragment == null) {
            transaction.add(R.id.container, targetFragment, targetFragment.javaClass.name)
            transaction.addToBackStack(targetFragment.javaClass.name)
            transaction.replace(R.id.container, targetFragment)
        } else {
            transaction.addToBackStack(targetFragment.javaClass.name)
            newFragment.arguments = targetFragment.arguments
            transaction.replace(R.id.container, newFragment)

        }

        transaction.commitAllowingStateLoss()
    }

    override fun onBackPress() {
        if (supporFragmentManager!!.backStackEntryCount > 1) {
            supporFragmentManager!!.popBackStackImmediate()
        } else
            ahBottomNavigation!!.currentItem = 0

        if ((supporFragmentManager!!.fragments.last() is OzlemCercFragment) or (supporFragmentManager!!.fragments.last() is PresidentFragment)
                or (supporFragmentManager!!.fragments.last() is PresidentGalleryFragment))
        {
            ahBottomNavigation!!.hideBottomNavigation(true)
        }
        else
        {
            ahBottomNavigation!!.restoreBottomNavigation(true)
        }
    }

    companion object {

        private var supporFragmentManager: FragmentManager? = null
        private var ahBottomNavigation: AHBottomNavigation? = null
    }


}
