package com.atum.bel.common.base;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;

public interface ChangingFragment {

    void init(AHBottomNavigation ahBottomNavigation, FragmentManager supporFragmentManager);
    void openFragment(Fragment tatgetFragment,String tag);
    void onBackPress();
}
