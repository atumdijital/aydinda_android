package com.atum.bel.api


interface ApiResultListener {

     fun succeed(fromJson: String)
     fun fail(errorMessage: String?)
}