package com.atum.bel.api

import android.content.Context
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson


abstract class ApiClient(val context: Context, val listener: ApiResultListener) {

    val baseUrl: String = "http://128.199.62.148:8000/"

    val queue = Volley.newRequestQueue(context)


    fun createGetRequest(prefix: String) {
        val stringRequest = StringRequest(Request.Method.GET, baseUrl.plus(prefix),
                Response.Listener<String> { response ->

                    try {
                        listener.succeed(response)

                    } catch (e: Exception) {
                        val gson =  Gson()

                        val result = gson.fromJson(response, ResponseModel::class.java)

                        listener.fail(result.message)

                    }
                },
                Response.ErrorListener {
                    listener.fail(it.message)
                })

        queue.add(stringRequest)
    }

    fun createPostRequest(body: HashMap<String,String>, prefix: String) {

        val postRequest = object : StringRequest(Method.POST,  baseUrl.plus(prefix),
                Response.Listener { response ->
                    // response
                    listener.succeed(response)
                },
                Response.ErrorListener {
                    // error
                    listener.fail(it.message)
                }
        ) {
            override fun getParams(): Map<String, String> {


                return body
            }
        }
        queue.add(postRequest)


    }


}