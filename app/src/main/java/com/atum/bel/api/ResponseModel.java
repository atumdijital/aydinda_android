package com.atum.bel.api;

import com.google.gson.annotations.SerializedName;

public class ResponseModel<T> {
	@SerializedName("result")
	private T result ;
	private int key;
	@SerializedName("message")
	private String message;

public int getKey () {
	return key;
}

public void setKey ( int key ) {
	this.key = key;
}

public String getMessage () {
	return message;
}

public void setMessage ( String message ) {
	this.message = message;
}

public T getResult () {
	return result;
}

public void setResult ( T result ) {
	this.result = result;
}
}
