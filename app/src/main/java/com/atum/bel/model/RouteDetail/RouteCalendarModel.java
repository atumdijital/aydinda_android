package com.atum.bel.model.RouteDetail;

import java.util.List;

public class RouteCalendarModel {
	private String title;
	private List<List<RouteLineModel>> weekday;
	private List<List<RouteLineModel>> saturday;
	private List<List<RouteLineModel>> sunday;

public String getTitle () {
	return title;
}

public void setTitle ( String title ) {
	this.title = title;
}

public List<List<RouteLineModel>> getWeekday () {
	return weekday;
}

public void setWeekday ( List<List<RouteLineModel>> weekday ) {
	this.weekday = weekday;
}

public List<List<RouteLineModel>> getSaturday () {
	return saturday;
}

public void setSaturday ( List<List<RouteLineModel>> saturday ) {
	this.saturday = saturday;
}

public List<List<RouteLineModel>> getSunday () {
	return sunday;
}

public void setSunday ( List<List<RouteLineModel>> sunday ) {
	this.sunday = sunday;
}
}
