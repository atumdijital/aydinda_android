package com.atum.bel.model.Project;

import java.util.List;

public class ProjectListModel {
	private List<ProjectModel> contiuning;
private List<ProjectModel> investment;
private List<ProjectModel> complated;

public List<ProjectModel> getContiuning () {
	return contiuning;
}

public void setContiuning ( List<ProjectModel> contiuning ) {
	this.contiuning = contiuning;
}

public List<ProjectModel> getInvestment () {
	return investment;
}

public void setInvestment ( List<ProjectModel> investment ) {
	this.investment = investment;
}

public List<ProjectModel> getComplated () {
	return complated;
}

public void setComplated ( List<ProjectModel> complated ) {
	this.complated = complated;
}
}
