package com.atum.bel.model.RouteDetail;

public class RouteLineModel {

	private String title;
	private String description;
	private String _id;

public String getTitle () {
	return title;
}

public void setTitle ( String title ) {
	this.title = title;
}

public String getDescription () {
	return description;
}

public void setDescription ( String description ) {
	this.description = description;
}

public String get_id () {
	return _id;
}

public void set_id ( String _id ) {
	this._id = _id;
}
}
