package com.atum.bel.model.Municipality

import com.atum.bel.api.ResponseModel

data class MunicipalityModel(val title:String, val webSite: String)
{
    class MunicipalityResponseModel : ResponseModel<MutableList<MunicipalityModel>>()

}