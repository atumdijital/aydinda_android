package com.atum.bel.model

import com.atum.bel.api.ResponseModel

data class EventModel(val title: String,
                      val desc: String,
                      val image_list: List<String>,
                      val location: List<Double>,
                      val address : String,
                      val start_at: Long,
                      val end_at: Long,
                      val status: String,
                      val id: String) {

    inner class EventResponseModel : ResponseModel<MutableList<EventModel>>()
}