package com.atum.bel.model.Pharmacy

import com.atum.bel.api.ResponseModel

data class PharmacyModel (val _id:String
                            , val title:String
                            , val address:String){
    class Response:ResponseModel<MutableList<PharmacyModel>>()
}