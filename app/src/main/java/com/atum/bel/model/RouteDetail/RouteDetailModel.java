package com.atum.bel.model.RouteDetail;

public class RouteDetailModel {
	private String id;
	private int lineNumber;
	private String routeGoing;
	private String routeReturn;
	private RouteCalendarModel routeGoingTime;
	private RouteCalendarModel routeReturnTime;


public String getId () {
	return id;
}

public void setId ( String id ) {
	this.id = id;
}

public int getLineNumber () {
	return lineNumber;
}

public void setLineNumber ( int lineNumber ) {
	this.lineNumber = lineNumber;
}

public String getRouteGoing () {
	return routeGoing;
}

public void setRouteGoing ( String routeGoing ) {
	this.routeGoing = routeGoing;
}

public String getRouteReturn () {
	return routeReturn;
}

public void setRouteReturn ( String routeReturn ) {
	this.routeReturn = routeReturn;
}

public RouteCalendarModel getRouteGoingTime () {
	return routeGoingTime;
}

public void setRouteGoingTime ( RouteCalendarModel routeGoingTime ) {
	this.routeGoingTime = routeGoingTime;
}

public RouteCalendarModel getRouteReturnTime () {
	return routeReturnTime;
}

public void setRouteReturnTime ( RouteCalendarModel routeReturnTime ) {
	this.routeReturnTime = routeReturnTime;
}
}
