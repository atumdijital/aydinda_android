package com.atum.bel.model.news

import com.atum.bel.api.ResponseModel

class NewsModel (val title : String,val desc : String,val image_list : List<String>, val create_time:Long) {

    class NewsResponseModel : ResponseModel<MutableList<NewsModel>?>()

}