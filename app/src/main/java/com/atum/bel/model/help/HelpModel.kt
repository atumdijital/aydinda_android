package com.atum.bel.model.help

import com.atum.bel.api.ResponseModel

data class HelpModel ( val title:String , val phone: String){

    class HelpResponseModel : ResponseModel<MutableList<HelpModel>>()
}