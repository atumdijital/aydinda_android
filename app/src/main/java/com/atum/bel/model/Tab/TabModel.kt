package com.atum.bel.model.Tab

data class TabModel (
        val title: String,
        val icon:String,
        val screen: String
        )

