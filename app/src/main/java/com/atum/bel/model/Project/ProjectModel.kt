package com.atum.bel.model.Project

class ProjectModel(val title:String,
                   val description : String,
                   val location : FloatArray,
                   val image_list : ArrayList<String>,
                   val proejct_type : Int) {
}