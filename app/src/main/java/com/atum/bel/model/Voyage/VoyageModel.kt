package com.atum.bel.model.Voyage

data class VoyageModel(val id: String ,
                       val lineNumber:Int,
                       val routeGoing:String,
                       val routeReturn:String) {}

