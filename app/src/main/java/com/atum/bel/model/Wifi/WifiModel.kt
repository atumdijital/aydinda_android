package com.atum.bel.model.Wifi

import com.atum.bel.api.ResponseModel

data class WifiModel (val id : String,
                      val location: DoubleArray,
                      var description: String)
{
    class WifiResponseModel : ResponseModel<MutableList<WifiModel>>()
}