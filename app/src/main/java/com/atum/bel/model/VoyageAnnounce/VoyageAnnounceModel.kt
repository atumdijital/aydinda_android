package com.atum.bel.model.VoyageAnnounce

data class VoyageAnnounceModel (
        val _id:String,
        val title:String,
        val status: Boolean
    )
