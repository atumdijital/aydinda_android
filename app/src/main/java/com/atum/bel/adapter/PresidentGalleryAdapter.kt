package com.atum.bel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.atum.bel.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_image_gallery.view.*

class PresidentGalleryAdapter(val list: MutableList<Int>,private val onItemClickListener: PresidentGalleryAdapter.OnItemClickListener): RecyclerView.Adapter<PresidentGalleryViewHolder>() {
    interface OnItemClickListener {
        fun onItemClick(position: Int)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PresidentGalleryViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)

        val view = inflater.inflate(R.layout.item_president_grid,parent,false)

        return PresidentGalleryViewHolder(view)

    }


    override fun getItemCount(): Int {
        return list.size
    }


    override fun onBindViewHolder(holder: PresidentGalleryViewHolder, position: Int) {

        Glide.with(holder.itemView).load(list.get(position)).into(holder.itemView.image)

        holder.itemView.setOnClickListener {
            onItemClickListener.onItemClick(position)
        }
    }
}

class PresidentGalleryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


