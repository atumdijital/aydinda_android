package com.atum.bel.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.atum.bel.R;
import com.atum.bel.common.ListType;
import com.atum.bel.model.Project.ProjectModel;
import com.bumptech.glide.Glide;

import java.util.List;

public class ProjectsSmallAdapter extends RecyclerView.Adapter<ProjectsSmallAdapter.ViewHolder> {

    private List<ProjectModel> list;
    private OnItemClickListener onItemClickListener;
    private ListType listType;

    public ProjectsSmallAdapter(List<ProjectModel> list, ListType selectedListType, OnItemClickListener onItemClickListener) {
        this.list = list;
        this.onItemClickListener = onItemClickListener;
        this.listType = selectedListType;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;
        if (listType.equals(ListType.Small))
             view = layoutInflater.inflate(R.layout.item_small_proj_list,parent,false);
        else
             view = layoutInflater.inflate(R.layout.item_expanded_proj_list,parent,false);

        return new ProjectsSmallAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.txtProjName.setText(list.get(position).getTitle());

        Glide.with(holder.itemView).load(list.get(position).getImage_list().get(0)).into(holder.appCompatImageView);

        holder.cvProjectSmall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(list.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private AppCompatImageView appCompatImageView;
        private TextView txtProjName;
        private CardView cvProjectSmall;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            appCompatImageView = itemView.findViewById(R.id.appCompatImageView);
            txtProjName = itemView.findViewById(R.id.txtProjName);
            cvProjectSmall = itemView.findViewById(R.id.cvProjectSmall);
        }
    }


    public interface OnItemClickListener
    {
        void onItemClick(ProjectModel project);
    }
}
