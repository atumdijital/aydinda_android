package com.atum.bel.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.atum.bel.R;
import com.atum.bel.model.Tab.TabModel;
import com.bumptech.glide.Glide;

import java.util.List;

public class MainBottomAdapter extends RecyclerView.Adapter<MainBottomAdapter.VH> {

    private List<TabModel> list;
    private onItemClickListener onItemClickListener;

    public MainBottomAdapter(onItemClickListener onItemClickListener, List<TabModel> list) {
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }



    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.item_main_bottom,parent,false);

        return new VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, final int position) {

        Glide.with(holder.itemView).load(list.get(position).getIcon()).into(holder.imgItemType);

        holder.txtItemDesc.setText(list.get(position).getTitle());

        holder.bottomItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(list.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class VH extends RecyclerView.ViewHolder {

        private ImageView imgItemType;
        private TextView txtItemDesc;
        private CardView bottomItem;

        VH(@NonNull View itemView) {
            super(itemView);

            imgItemType = itemView.findViewById(R.id.imgItemType);
            txtItemDesc = itemView.findViewById(R.id.txtItemDesc);
            bottomItem = itemView.findViewById(R.id.bottomItem);
        }
    }

    public interface onItemClickListener
    {
        void onItemClick(TabModel model);
    }
}
