package com.atum.bel.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.atum.bel.R
import com.atum.bel.model.RouteDetail.RouteLineModel
import kotlinx.android.synthetic.main.item_voyage_time.view.*

class VoyageHourAdapter(val context : Context,val list: List<List<RouteLineModel>>): RecyclerView.Adapter<VoyageHourAdapter.ViewH> (){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewH {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)

        val view = inflater.inflate(R.layout.item_voyage_time,parent,false)

        return VoyageHourAdapter.ViewH(view)
    }


    override fun onBindViewHolder(holder: ViewH, position: Int) {
        holder.itemView.rvHours.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        holder.itemView.rvHours.adapter = VoyageTimeAdapter(list.get(position))
    }


    override fun getItemCount(): Int {
      return  list.size
    }

    class ViewH(itemView: View) : RecyclerView.ViewHolder(itemView)
}