package com.atum.bel.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.atum.bel.R
import com.atum.bel.model.MainSlider.SliderModel
import java.util.*

class SliderAdapter(private val context: Context, private val sliderModelList: List<SliderModel>) : PagerAdapter() {
    private var header: TextView? = null
    private var desc: TextView? = null
    private var viewPager: ViewPager? = null

    override fun getCount(): Int {
        return sliderModelList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val view = Objects.requireNonNull(layoutInflater).inflate(R.layout.item_slider, null)

        header = view.findViewById(R.id.header)

        desc = view.findViewById(R.id.desc)

        header!!.text = sliderModelList[position].title

        desc!!.text = sliderModelList[position].description

        viewPager = container as ViewPager


        viewPager!!.addView(view, position)




        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        viewPager = container as ViewPager
        val view = `object` as View
        viewPager!!.removeView(view)
    }
}
