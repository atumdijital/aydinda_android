package com.atum.bel.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.atum.bel.view.wifi.WifiInfoFragment
import com.atum.bel.view.wifi.WifiMapFragment

class WifiPagerAdapter(val fm: FragmentManager ) : FragmentStatePagerAdapter(fm) {


    override fun getItem(position: Int): Fragment {

        when(position)
        {
            0-> return WifiMapFragment( )
            1 -> return WifiInfoFragment()

            else -> {
                return WifiMapFragment()
            }
        }

    }

    override fun getCount(): Int {


        return 2

    }
}