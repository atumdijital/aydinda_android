package com.atum.bel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.atum.bel.R
import com.atum.bel.model.help.HelpModel
import kotlinx.android.synthetic.main.item_help_list.view.*

class HelpContactsAdapter(val list: MutableList<HelpModel>, val itemClickListener: onItemClickListener): RecyclerView.Adapter<HelpContactsAdapter.VH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)

        val view = inflater.inflate(R.layout.item_help_list,parent,false)

        return VH(view)
    }

    override fun getItemCount(): Int {
      return  list.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.itemView.txtNumber.text = list.get(position).phone
        holder.itemView.txtTitle.text = list.get(position).title

        holder.itemView.setOnClickListener {
            itemClickListener.onItemClick(list.get(position))
        }
    }


    inner class  VH (itemView: View): RecyclerView.ViewHolder(itemView)

    interface onItemClickListener{
        fun onItemClick(item: HelpModel)

    }

}