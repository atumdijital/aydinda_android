package com.atum.bel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.atum.bel.R
import com.atum.bel.model.RouteDetail.RouteLineModel
import kotlinx.android.synthetic.main.item_voyage_hour.view.*

class VoyageTimeAdapter(val list : List<RouteLineModel>):RecyclerView.Adapter<VoyageTimeAdapter.ViewHold> () {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHold {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)

        val view = inflater.inflate(R.layout.item_voyage_hour,parent,false)

        return VoyageTimeAdapter.ViewHold(view)
    }

    override fun getItemCount(): Int {

        return list.size
    }


    override fun onBindViewHolder(holder: ViewHold, position: Int) {

        holder.itemView.txtTime.text = list.get(position).title

    }

    class ViewHold(itemView: View) : RecyclerView.ViewHolder(itemView)

}