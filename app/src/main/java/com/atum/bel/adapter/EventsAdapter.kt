package com.atum.bel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.atum.bel.R
import com.atum.bel.model.EventModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_event_list.view.*

class EventsAdapter(val list: MutableList<EventModel>): RecyclerView.Adapter<EventsAdapter.EventsVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsVH {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)

        val view = inflater.inflate(R.layout.item_event_list,parent,false)

        return EventsVH(view)
    }


    override fun getItemCount(): Int {
        return list.size
    }


    override fun onBindViewHolder(holder: EventsVH, position: Int) {
        val event = list.get(position)

        holder.itemView.txtEventName.text = event.title
        holder.itemView.txtEventTime.text = event.desc

        Glide.with(holder.itemView)
                .load(event.image_list[0])
                .apply(RequestOptions.circleCropTransform())
                .into(holder.itemView.ivEventImage)

        holder.itemView.btnMap.setOnClickListener {
            //TODO: EventMap Fragment
        }
    }


    inner class EventsVH(itemView:View) : RecyclerView.ViewHolder(itemView)

}