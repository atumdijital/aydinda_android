package com.atum.bel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.atum.bel.R
import com.atum.bel.model.Municipality.MunicipalityModel
import kotlinx.android.synthetic.main.item_services_list.view.*

class ServicesAdapter(val list: MutableList<MunicipalityModel>,val onItemClickListener: OnItemClickListener ): RecyclerView.Adapter<ServicesAdapter.ServicesVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServicesVH {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)

        val view = inflater.inflate(R.layout.item_services_list,parent,false)

        return ServicesVH(view)
    }


    override fun getItemCount(): Int {
        return list.size
    }


    override fun onBindViewHolder(holder: ServicesVH, position: Int) {
        holder.itemView.txtServiceName.text = list.get(position).title

        holder.itemView.setOnClickListener {
            onItemClickListener.onItemClick(list.get(position))
        }
    }


    inner class ServicesVH(itemView: View) : RecyclerView.ViewHolder(itemView)
    interface OnItemClickListener {
        fun onItemClick(item: MunicipalityModel)
    }
}