package com.atum.bel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.atum.bel.R
import com.atum.bel.model.Pharmacy.PharmacyModel
import kotlinx.android.synthetic.main.item_pharmacy.view.*

class PharmacyAdapter(val list: MutableList<PharmacyModel>, val onItemClickListener: PharmacyAdapter.OnItemClickListener) : RecyclerView.Adapter<PharmacyAdapter.PharmacyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PharmacyViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)

        val view = inflater.inflate(R.layout.item_pharmacy,parent,false)

        return PharmacyViewHolder(view)

    }


    override fun getItemCount(): Int {
       return list.size
    }


    override fun onBindViewHolder(holder: PharmacyViewHolder, position: Int) {
        val item = list.get(position)

        holder.itemView.txtPharmacyName.text = item.title
        holder.itemView.txtPharmacyAddress.text = item.address

        holder.itemView.setOnClickListener(
                {
                    onItemClickListener.onItemClick(item)

                }
        )

    }

    class PharmacyViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView)

    interface OnItemClickListener {
        fun onItemClick(pharmacy: PharmacyModel)
    }

}