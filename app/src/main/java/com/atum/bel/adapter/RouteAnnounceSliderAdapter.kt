package com.atum.bel.adapter

import android.view.ViewGroup
import android.widget.TextView
import com.atum.bel.R
import com.atum.bel.model.VoyageAnnounce.VoyageAnnounceModel
import com.opensooq.pluto.base.PlutoAdapter
import com.opensooq.pluto.base.PlutoViewHolder


class RouteAnnounceSliderAdapter(items: MutableList<VoyageAnnounceModel>)
    : PlutoAdapter<VoyageAnnounceModel, RouteAnnounceSliderAdapter.RouteAnnounceSliderViewHolder>(items) {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): RouteAnnounceSliderViewHolder {
        return RouteAnnounceSliderViewHolder(parent, R.layout.item_route_announce)
    }

    class RouteAnnounceSliderViewHolder(parent: ViewGroup, itemLayoutId: Int) : PlutoViewHolder<VoyageAnnounceModel>(parent, itemLayoutId) {
        val txtTitle: TextView = getView(R.id.txtTitle)

        override fun set(item: VoyageAnnounceModel, position: Int) {

            txtTitle.text = item.title
        }



    }
}