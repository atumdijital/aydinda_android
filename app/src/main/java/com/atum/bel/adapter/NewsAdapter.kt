package com.atum.bel.adapter

import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.atum.bel.R
import com.atum.bel.model.news.NewsModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_news_list.view.*
import java.util.*

class NewsAdapter(val list: MutableList<NewsModel>, val listener:NewsItemOnClickListener) : RecyclerView.Adapter<NewsAdapter.NewsVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsVH {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)

        val view = inflater.inflate(R.layout.item_news_list,parent,false)

        return NewsVH(view)    }


    override fun getItemCount(): Int {
        return list.size
    }


    override fun onBindViewHolder(holder: NewsVH, position: Int) {


        Glide.with(holder.view).load(list.get(position).image_list.get(0)).into(holder.itemView.ivNewsImage)

        holder.itemView.txtNewsTitle.text = list.get(position).title

        holder.itemView.txtDescription.text = list.get(position).desc


        holder.itemView.txtTime.text =
                DateUtils.getRelativeTimeSpanString(list.get(position).create_time * 1000,
                        Date().time,DateUtils.MINUTE_IN_MILLIS,DateUtils.FORMAT_ABBREV_RELATIVE)



        holder.view.setOnClickListener {
            listener.onClick(list.get(position))
        }
    }

    inner class NewsVH(val view: View) : RecyclerView.ViewHolder(view)


    interface NewsItemOnClickListener{
        fun onClick(it:NewsModel)
    }


}