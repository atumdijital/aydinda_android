package com.atum.bel.adapter;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.atum.bel.R;
import com.bumptech.glide.Glide;
import com.opensooq.pluto.base.PlutoAdapter;
import com.opensooq.pluto.base.PlutoViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class PresidenSlider extends PlutoAdapter<Integer, PresidenSlider.SliderHolder> {


public PresidenSlider ( @NotNull List<Integer> items ) {
	super(items);
}

@Override
public SliderHolder getViewHolder( ViewGroup parent, int viewType) {
	return new SliderHolder(parent, R.layout.item_image_gallery);
}

public static class SliderHolder extends PlutoViewHolder<Integer> {
	ImageView ivPoster;
	TextView tvRating;

	public SliderHolder(ViewGroup parent, int itemLayoutId) {
		super(parent, itemLayoutId);
		ivPoster = getView(R.id.image);
	}

	@Override
	public void set(Integer item, int pos) {
		  Glide.with(getContext()).load(item).into(ivPoster);
	}
}
}