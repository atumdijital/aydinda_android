package com.atum.bel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.atum.bel.R
import com.atum.bel.model.Voyage.VoyageModel
import kotlinx.android.synthetic.main.item_voyages_list.view.*

class VoyagesAdapter(val list: MutableList<VoyageModel>?, private val onItemClickListener: OnItemClickListener) : RecyclerView.Adapter<VoyagesAdapter.VH>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {

        val inflater:LayoutInflater = LayoutInflater.from(parent.context)

        val view = inflater.inflate(R.layout.item_voyages_list,parent,false)

        return VH(view)
    }

    override fun getItemCount(): Int {

        return list!!.size

    }

    override fun onBindViewHolder(holder: VH, position: Int) {

        holder.itemView.txtFrom.text = list!!.get(position).routeGoing
        holder.itemView.txtTo.text = list.get(position).routeReturn
        holder.itemView.txtVoyageNumber.text = list.get(position).lineNumber.toString()



        holder.itemView.setOnClickListener {

            onItemClickListener.onItemClick(list.get(position))
        }



    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView)


    interface OnItemClickListener {
        fun onItemClick(obje: VoyageModel)
    }




}


