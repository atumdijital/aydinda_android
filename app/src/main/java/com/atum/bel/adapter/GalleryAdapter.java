package com.atum.bel.adapter;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.atum.bel.R;
import com.bumptech.glide.Glide;
import com.opensooq.pluto.base.PlutoAdapter;
import com.opensooq.pluto.base.PlutoViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class GalleryAdapter extends PlutoAdapter<String, GalleryAdapter.GalleryViewHolder> {


public GalleryAdapter ( @NotNull List<String> items ) {
	super(items);
}


@Override
public GalleryViewHolder getViewHolder( ViewGroup parent, int viewType) {
	return new GalleryViewHolder(parent, R.layout.item_image_gallery);
}

public static class GalleryViewHolder extends PlutoViewHolder<String> {
	ImageView ivPoster;
	TextView tvRating;

	public GalleryViewHolder(ViewGroup parent, int itemLayoutId) {
		super(parent, itemLayoutId);
		ivPoster = getView(R.id.image);
	}


	@Override
	public void set ( String s, int i ) {
		Glide.with(getContext()).load(s).into(ivPoster);

	}
}
}
